<?php

namespace System;

class Routers
{
    public static function load($app, $container)
    {
        $open = opendir(ROOT_DIR . getenv('ROUTES_DIR'));
        while ($file = readdir($open)) {
            if ($file != "." and $file != "..") {
                include ROOT_DIR . getenv('ROUTES_DIR') . '/' . $file;
            }
        }
    }
}