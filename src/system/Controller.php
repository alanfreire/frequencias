<?php

namespace System;

use Slim\Http\Request;
use Slim\Http\Response;

class Controller
{
    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function __get($name)
    {
        return $this->container->{$name} ?? null;
    }

    protected function offset($page)
    {
        return $page <= 1? 0: ($page - 1) * getenv('APP_LIMIT_PAGINATION');
    }


    public function postSearch($request, $response)
    {
        if (false === $request->getAttribute('csrf_status')) {
            $this->flash->addMessage('error', 'Erro ao realizar a requisição, tente novamente.');
            return $response->withRedirect($this->router->pathFor($this->page));
        }

        $_SESSION[$this->page]['search'] = $request->getParam('search');

        return $response->withRedirect($this->router->pathFor($this->page));
    }

    public function getClearSearch($request, $response)
    {
        unset($_SESSION[$this->page]['search']);

        return $response->withRedirect($this->router->pathFor($this->page));
    }

    public function validateCsrf(Request $request, Response $response)
    {
        if (false === $request->getAttribute('csrf_status')) {
            $this->flash->addMessage('error', 'Erro ao realizar a requisição, tente novamente.');
            return $response->withRedirect($this->router->pathFor($this->page));
        }

        return;
    }
}