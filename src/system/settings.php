<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Monolog settings
        'logger' => [
            'name' => 'Sistema-de-Controle-de-Frequencias',
            'path' => isset($_ENV['docker']) ? 'php://stdout' : ROOT_DIR . getenv('LOGS_DIR') . '/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
    ],
    'data' => [
        'title' => getenv('APP_TITLE'),
        'url' => getenv('APP_URL'),
        'nivel' => [1 => 'Gerenciador', 2 => 'Alimentador', 3 => 'Visualizador'],
        'semana' => [1 => 'Segunda-feira', 2 => 'Terça-feira', 3 => 'Quarta-feira', 4 => 'Quinta-feira', 5 => 'Sexta-feira'],
        'meses' => [
            1 => 'Janeiro',
            2 => 'Fevereiro',
            3 => 'Março',
            4 => 'Abril',
            5 => 'Maio',
            6 => 'Junho',
            7 => 'Julho',
            8 => 'Agosto',
            9 => 'Setembro',
            10 => 'Outubro',
            11 => 'Novembro',
            12 => 'Dezembro',
        ],
        'semana' => [
            'DOM',
            'SEG',
            'TER',
            'QUA',
            'QUI',
            'SEX',
            'SAB',
        ],
        'semanaAll' => [
            'DOMINGO',
            'SEGUNDA',
            'TERÇA',
            'QUARTA',
            'QUINTA',
            'SEXTA',
            'SÁBADO',
        ],
    ],
];
