<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Frequencias extends Model
{
    protected $table = 'frequencias_recebidas';

    protected $fillable = [
        'data_entrega',
        'frequencia',
        'observacoes',
        'servidores_id',
    ];

    public function servidores()
    {
        return $this->belongsTo('App\Models\Servidores');
    }
}