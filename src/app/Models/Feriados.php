<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feriados extends Model
{
    protected $table = 'feriados';

    protected $fillable = [
        'data',
        'feriados_tipos_id',
    ];

    public function feriadosTipos()
    {
        return $this->belongsTo('App\Models\FeriadosTipos');
    }
}