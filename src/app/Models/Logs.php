<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Logs extends Model
{
    protected $table = 'logs';

    protected $fillable = [
        'conteudo',
        'usuarios_id',
    ];

    public static function set($content, $user = null)
    {
        $user = $user ?? $_SESSION['user'];
        try {
            $log = new Logs;
            $log->conteudo = $content;
            $log->usuarios_id = $user;
            $log->save();
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    public function usuarios()
    {
        return $this->belongsTo('App\Models\Usuarios');
    }
}