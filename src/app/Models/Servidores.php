<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Servidores extends Model
{
    protected $table = 'servidores';

    protected $fillable = [
        'nome',
        'matricula',
        'jornada',
        'cargos_id',
        'campus_id',
        'funcoes_id',
    ];

    public function cargos()
    {
        return $this->belongsTo('App\Models\Cargos');
    }

    public function campus()
    {
        return $this->belongsTo('App\Models\Campus');
    }

    public function funcoes()
    {
        return $this->belongsTo('App\Models\Funcoes');
    }
}