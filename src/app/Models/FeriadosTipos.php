<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FeriadosTipos extends Model
{
    protected $table = 'feriados_tipos';

    protected $fillable = [
        'descricao',
    ];
}