<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Funcoes extends Model
{
    protected $table = 'funcoes';

    protected $fillable = [
        'descricao',
        'referencia',
    ];
}