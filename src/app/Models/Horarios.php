<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Horarios extends Model
{
    protected $table = 'horarios';

    protected $fillable = [
        'semana',
        'entrada_m',
        'saida_m',
        'entrada_t',
        'saida_m',
        'um_turno',
        'servidores_id',
    ];

    public function servidores()
    {
        return $this->belongsTo('App\Models\Servidores');
    }
}