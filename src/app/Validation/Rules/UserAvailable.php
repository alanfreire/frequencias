<?php

namespace App\Validation\Rules;

use App\Models\Usuarios;
use Respect\Validation\Rules\AbstractRule;


class UserAvailable extends AbstractRule
{
    public function validate($input)
    {
        return Usuarios::where('usuario', $input)->count() === 0;
    }
}