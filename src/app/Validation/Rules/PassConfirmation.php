<?php

namespace App\Validation\Rules;

use Respect\Validation\Rules\AbstractRule;

class PassConfirmation extends AbstractRule
{
    public $compareTo;

    public function __construct($compareTo)
    {
        $this->compareTo = $compareTo;
    }

    public function validate($input)
    {
        return $input == $this->compareTo;
    }
}