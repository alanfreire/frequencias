<?php

namespace App\Validation\Rules;

use App\Models\Frequencias;
use Respect\Validation\Rules\AbstractRule;

class FrequenciaAvailable extends AbstractRule
{
    public $ano;
    public $servidor;

    public function __construct($ano, $servidor)
    {
        $this->ano = $ano;
        $this->servidor = $servidor;
    }

    public function validate($input)
    {
        return Frequencias::where('frequencia', sprintf('%04d-%02d-01', $this->ano, $input))
                ->where('servidores_id', $this->servidor)
                ->count() === 0;
    }
}