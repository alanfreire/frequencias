<?php

namespace App\Helpers\Bootstrap;


use HTMLBuilder\ElementFactory as El;

class Alerts
{
    private static $type;
    private static $message;

    public static function render($type, $message = '')
    {
        if (null === static::parse($type, $message)) {
            return null;
        }

        $alert = El::make('div')->attr('class', ['alert', 'alert-'.static::$type])
                                      ->attr('role', ['alert'])
                                      ->value(static::button())
                                      ->value(El::make('h4')->attr('class', ['alert-heading'])->value('Aviso!'))
                                      ->value(El::make('hr'))
                                      ->value(El::make('p')->value(static::$message));

        return $alert->render();
    }

    private static function parse($type, $message)
    {
        if (null === $type) {
            return null;
        }

        static::$type = is_object($type)? $type->type: $type;
        static::$message = is_object($type)? $type->message: $message;

        return true;
    }

    private static function button()
    {
        return El::make('button')->attr('type', ['button'])
                                       ->attr('class', ['close'])
                                       ->attr('data-dismiss', ['alert'])
                                       ->attr('aria-label', ['Fechar'])
                                       ->value(El::make('span')->attr('aria-hidden', [true])->value('&times;'));
    }
}