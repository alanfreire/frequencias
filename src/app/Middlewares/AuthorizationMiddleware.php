<?php
namespace App\Middlewares;

use System\Middleware;

class AuthorizationMiddleware extends Middleware
{
    private $level = [];

    public function __construct($container, $level = [1, 2, 3])
    {
        parent::__construct($container);
        $this->setLevel($level);
    }

    public function __invoke($request, $response, $next)
	{
		if (!in_array($_SESSION['user_lvl'], $this->getLevel())) {
			$this->container->flash->addMessage('error', 'Você não tem autorização para acessar essa página.');
			return $response->withRedirect($this->container->router->pathFor('admin'));
		}

		$response = $next($request, $response);
		return $response;
	}

	public function setLevel($level)
    {
        $level = is_array($level)? $level: [$level];
        $this->level = array_merge($level, $this->level);
        return $this;
    }

    public function getLevel()
    {
        return $this->level;
    }
}