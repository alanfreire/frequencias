<?php

namespace App\Auth;

use App\Models\Usuarios;

class Auth
{
    public function user()
    {
        if (isset($_SESSION['user'])) {
            return Usuarios::find($_SESSION['user']);
        }

        return null;
    }

    public function check()
    {
        return isset($_SESSION['user']);
    }

    public function attempt($user, $pass)
    {
        $user = Usuarios::where('usuario' , $user)->first();

        if (!$user || !$user->situacao) {
            return false;
        }

        if (password_verify($pass, $user->senha)) {
            $_SESSION['user'] = $user->id;
            $_SESSION['user_lvl'] = $user->nivel;
            return true;
        }

        return false;
    }

    public function logout()
    {
        unset($_SESSION['user']);
        session_destroy();

        return;
    }
}