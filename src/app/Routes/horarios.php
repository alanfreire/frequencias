<?php

$app->group('/admin/horarios', function () use ($app) {
    $controller = 'App\Controllers\Admin\HorariosController';
    $app->get('/{servidor}', $controller.':index')->setName('horarios');
    $app->post('/{servidor}', $controller.':postCreate');
    $app->get('/{servidor}/apagar/{id}[/{page}]', $controller.':getDelete');
    $app->get('/{servidor}/editar/{id}[/{page}]', $controller.':getEdit');
    $app->post('/{servidor}/editar/{id}[/{page}]', $controller.':postEdit');
    $app->post('/{servidor}/buscar/por[/{page}]', $controller.':postSearch');
    $app->get('/{servidor}/buscar/limpar[/]', $controller.':getClearSearch');
})->add(new \App\Middlewares\AuthMiddleware($container))->add(new \App\Middlewares\AuthorizationMiddleware($container, [1, 2, 3]));