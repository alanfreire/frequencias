<?php

$app->group('/admin/campus', function () use ($app) {
    $controller = 'App\Controllers\Admin\CampusController';
    $app->get('[/{page}]', $controller.':index')->setName('campus');
    $app->post('[/{page}]', $controller.':postCreate');
    $app->get('/apagar/{id}[/{page}]', $controller.':getDelete');
    $app->get('/editar/{id}[/{page}]', $controller.':getEdit');
    $app->post('/editar/{id}[/{page}]', $controller.':postEdit');
    $app->post('/buscar/por[/{page}]', $controller.':postSearch');
    $app->get('/buscar/limpar[/]', $controller.':getClearSearch');
})->add(new \App\Middlewares\AuthMiddleware($container))->add(new \App\Middlewares\AuthorizationMiddleware($container, [1, 2, 3]));