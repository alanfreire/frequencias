<?php

$app->group('/admin/usuarios', function () use ($app) {
    $controller = 'App\Controllers\Admin\UsuariosController';
    $app->get('[/{page}]', $controller.':index')->setName('usuarios');
    $app->post('[/{page}]', $controller.':postCreate');
    $app->get('/apagar/{id}[/{page}]', $controller.':getDelete');
    $app->get('/editar/{id}[/{page}]', $controller.':getEdit');
    $app->post('/editar/{id}[/{page}]', $controller.':postEdit');
    $app->post('/buscar/por[/{page}]', $controller.':postSearch');
    $app->get('/buscar/limpar[/]', $controller.':getClearSearch');
    $app->post('/senha/{id}[/]', $controller.':postPassword');
})->add(new \App\Middlewares\AuthMiddleware($container))->add(new \App\Middlewares\AuthorizationMiddleware($container, [1,2]));