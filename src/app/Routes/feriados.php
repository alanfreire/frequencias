<?php

$app->group('/admin/feriados', function () use ($app) {
    $controller = 'App\Controllers\Admin\FeriadosController';
    $app->get('[/{page}]', $controller.':index')->setName('feriados');
    $app->post('[/{page}]', $controller.':postCreate');
    $app->get('/apagar/{id}[/{page}]', $controller.':getDelete');
    $app->get('/editar/{id}[/{page}]', $controller.':getEdit');
    $app->post('/editar/{id}[/{page}]', $controller.':postEdit');
    $app->post('/buscar/por[/{page}]', $controller.':postSearch');
    $app->get('/buscar/limpar[/]', $controller.':getClearSearch');
})->add(new \App\Middlewares\AuthMiddleware($container))->add(new \App\Middlewares\AuthorizationMiddleware($container, [1, 2, 3]));