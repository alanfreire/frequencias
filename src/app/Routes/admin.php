<?php

$app->get('/admin', 'App\Controllers\Admin\AdminController:index')
    ->setName('admin')
    ->add(new \App\Middlewares\AuthMiddleware($container));