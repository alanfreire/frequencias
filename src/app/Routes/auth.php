<?php
use App\Middlewares\GuestMiddleware;

$app->group('/login', function () use ($app, $container) {
    $app->get('[/{key}]', 'App\Controllers\AuthController:getSignIn')->setName('auth.signin')->add(new GuestMiddleware($container));
    $app->post('[/{key}]', 'App\Controllers\AuthController:postSignIn')->add(new GuestMiddleware($container));
});

$app->get('/sair', 'App\Controllers\AuthController:getSignOut')->setName('auth.signout');