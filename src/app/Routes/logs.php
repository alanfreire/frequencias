<?php

$app->group('/admin/logs', function () use ($app) {
    $app->get('/{usuario}[/{page}]', 'App\Controllers\Admin\LogsController:index')->setName('logs');
    $app->post('/{usuario}[/{page}]', 'App\Controllers\Admin\LogsController:postUser');
    $app->get('/{usuario}/apagar/{id}', 'App\Controllers\Admin\LogsController:getDelete');
})->add(new \App\Middlewares\AuthMiddleware($container))->add(new \App\Middlewares\AuthorizationMiddleware($container, [1]));