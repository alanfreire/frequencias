<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes

$app->get('/[{matricula}/]', 'App\Controllers\HomeController:index')->setName('home');
$app->post('/[{matricula}/]', 'App\Controllers\HomeController:post');
$app->get('/imprimir/{mes}/{ano}/{matricula}[/]', 'App\Controllers\HomeController:getPrint')->setName('print');
$app->get('/imprimir/html/{mes}/{ano}/{matricula}[/]', 'App\Controllers\HomeController:getHtml')->setName('frequencia.html');