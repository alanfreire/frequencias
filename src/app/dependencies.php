<?php
// DIC configuration

$container = $app->getContainer();

// database
$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection([
    'driver' => getenv('DB_DRIVER'),
    'host' => getenv('DB_HOST'),
    'database' => getenv('DB_NAME'),
    'username' => getenv('DB_USER'),
    'password' => getenv('DB_PASS'),
    'charset' => getenv('DB_CHARSET'),
    'collation' => getenv('DB_COLLATION'),
    'prefix' => getenv('DB_PREFIX'),
]);
$capsule->setAsGlobal();
$capsule->bootEloquent();

$container['db'] = function () use ($capsule) {
    return $capsule;
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

// flash sessions
$container['flash'] = function () {
    return new \Slim\Flash\Messages;
};

// auth
$container['auth'] = function () {
    return new \App\Auth\Auth;
};

// authorization
$container['authorization'] = function ($container) {
    return new \App\Middlewares\AuthorizationMiddleware($container);
};

// Validation
$container['validator'] = function () {
    return new \App\Validation\Validator;
};

Respect\Validation\Validator::with('App\\Validation\\Rules');

// Register component on container
$container['view'] = function ($container) use ($settings) {
    $view = new \Slim\Views\Twig(ROOT_DIR . getenv('VIEWS_DIR'), [
        'cache' => false, #ROOT_DIR . getenv('VIEWS_DIR') . '/cache',
        'debug' => true,
    ]);

    // Instantiate and add Slim specific extension
    $router = $container->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new Slim\Views\TwigExtension($router, $uri));
    $view->addExtension(new Twig_Extension_Debug());

    $view->getEnvironment()->addGlobal('auth', [
        'check' => $container->auth->check(),
        'user' => $container->auth->user(),
    ]);

    $view->getEnvironment()->addGlobal('flash', $container->flash);
    $view->getEnvironment()->addGlobal('data', $settings['data']);

    if (isset($_SESSION['user_lvl'])) {
        $view->getEnvironment()->addGlobal('nivel', $_SESSION['user_lvl']);
    }

    return $view;
};

// CSRF protection
$container['csrf'] = function(){
    $guard = new \Slim\Csrf\Guard;

    $guard->setFailureCallable(function ($request, $response, $next) {
        $request = $request->withAttribute("csrf_status", false);
        return $next($request, $response);
    });

    return $guard;
};


// Controllers
$container['HomeController'] = function($container) {
    return new App\Controllers\HomeController($container);
};

$container['AdminController'] = function($container) {
    return new App\Controllers\Admin\AdminController($container);
};

$container['UsuariosController'] = function($container) {
    return new App\Controllers\Admin\UsuariosController($container);
};

$container['AuthController'] = function($container) {
    return new App\Controllers\AuthController($container);
};

// Securety sessions
$container['sessionMiddleware'] = function () {
    return new \Slim\Middleware\Session([
        'name' => getenv('APP_SESSION_NAME'),
        'lifetime' => getenv('APP_SESSION_EXPIRE'),
        'autorefresh' => true
    ]);
};
