<?php

namespace App\Controllers\Admin;


use App\Models\Servidores;
use System\Controller;
use App\Models\Frequencias;
use App\Models\Logs;
use App\Helpers\Bootstrap\Pagination;
use Respect\Validation\Validator as v;

class FrequenciasController extends Controller
{
    public function __construct($container)
    {
        parent::__construct($container);
        $this->page = 'frequencias';
    }

    public function index($request, $response, $args)
    {
        $numOfPage = $args['page'] ?? 1;
        $data['tuplas'] = $this->all($this->offset($numOfPage));
        $data['servidores'] = Servidores::orderBy('nome', 'asc')->get();
        $data['pagination'] = Pagination::render($this->count(), $args['page'] ?? 1, $this->router->pathFor($this->page));
        $data['search'] = $_SESSION[$this->page]['search'] ?? false;

        return $this->view->render($response, 'admin/'.$this->page.'/index.twig', $data);
    }

    public function postCreate($request, $response)
    {
        if (false === $request->getAttribute('csrf_status')) {
            $this->flash->addMessage('error', 'Erro ao realizar a requisição, tente novamente.');
            return $response->withRedirect($this->router->pathFor($this->page));
        }

        $validation = $this->validator->validate($request, [
            'data_entrega'  => v::date('d/m/Y'),
            'mes'           => v::frequenciaAvailable($request->getParam('ano'), $request->getParam('servidores_id'))->date('m'),
            'ano'           => v::date('Y'),
            'servidores_id' => v::intVal()->notEmpty(),
        ]);

        if ($validation->failed()) {
            return $response->withRedirect($this->router->pathFor($this->page));
        }

        try {
            $reg = new Frequencias();
            $reg->data_entrega  = implode('-', array_reverse(explode('/', $request->getParam('data_entrega'))));
            $reg->frequencia  = date('Y-m-d', strtotime($request->getParam('ano').'-'.$request->getParam('mes').'-01'));
            $reg->servidores_id  = $request->getParam('servidores_id');
            $reg->observacoes  = $request->getParam('observacoes');
            $reg->save();
        } catch (\Exception $e) {
            die($e->getMessage());
        }

        Logs::set("O usuário [{$this->auth->user()->usuario}] criou um novo registro de Frequencias.");

        $this->flash->addMessage('success', 'Registro salvo com sucesso!');

        return $response->withRedirect($this->router->pathFor($this->page));
    }

    public function getDelete($request, $response, $args)
    {
        $id = (int) $args['id'];

        $reg = Frequencias::find($id);

        if (!$reg) {
            $this->flash->addMessage('error', 'Erro ao realizar a requisição, tente novamente.');
            return $response->withRedirect($this->router->pathFor($this->page));
        }

        $reg->delete();

        Logs::set("O usuário [{$this->auth->reg()->usuario}] apagou um registro de Frequencias.");

        $this->flash->addMessage('success', 'Registro apagado com sucesso!');

        return $response->withRedirect($this->router->pathFor($this->page));
    }

    public function getEdit($request, $response, $args)
    {
        $id = (int) $args['id'];
        $numOfPage = $args['page'] ?? 1;
        $data['tuplas'] = $this->all($this->offset($numOfPage));
        $data['edit'] = $this->readEdit($id);
        $data['servidores'] = Servidores::orderBy('nome', 'asc')->get();
        $data['form'] = true;
        $data['pagination'] = Pagination::render($this->count(), $args['page'] ?? 1, $this->router->pathFor($this->page));
        return $this->view->render($response, 'admin/'.$this->page.'/index.twig', $data);
    }

    private function readEdit($frequencia)
    {
        $frequencia = Frequencias::find($frequencia);
        if (!$frequencia) {
            return null;
        }

        $tmp['id'] = $frequencia->id;
        $tmp['data_entrega'] = $frequencia->data_entrega;
        $tmp['mes'] = date('m', strtotime($frequencia->frequencia));
        $tmp['ano'] = date('Y', strtotime($frequencia->frequencia));
        $tmp['servidores_id'] = $frequencia->servidores_id;
        return $tmp;
    }

    public function postEdit($request, $response, $args)
    {
        if (false === $request->getAttribute('csrf_status')) {
            $this->flash->addMessage('error', 'Erro ao realizar a requisição, tente novamente.');
            return $response->withRedirect($this->router->pathFor($this->page)."/editar/{$args['id']}");
        }

        $id = (int) $args['id'];

        $reg = Frequencias::find($id);

        if (!$reg) {
            $this->flash->addMessage('error', 'Erro ao realizar a requisição, tente novamente.');
            return $response->withRedirect($this->router->pathFor($this->page));
        }

        $validation = $this->validator->validate($request, [
            'data_entrega'  => v::date('d/m/Y'),
            'mes'           => v::date('m'),
            'ano'           => v::date('Y'),
            'servidores_id' => v::intVal()->notEmpty(),
        ]);

        if ($validation->failed()) {
            return $response->withRedirect($this->router->pathFor($this->page));
        }

        $reg->data_entrega  = implode('-', array_reverse(explode('/', $request->getParam('data_entrega'))));
        $reg->frequencia  = date('Y-m-d', strtotime($request->getParam('ano').'-'.$request->getParam('mes').'-01'));
        $reg->servidores_id  = $request->getParam('servidores_id');
        $reg->observacoes  = $request->getParam('observacoes');
        $reg->save();

        Logs::set("O usuário [{$this->auth->user()->usuario}] editou um registro de Frequencias");

        $this->flash->addMessage('success', 'Registro atualizado com sucesso!');

        return $response->withRedirect($this->router->pathFor($this->page));
    }

    private function all($offset = 0)
    {
        if (isset($_SESSION[$this->page]['search'])) {
            return Frequencias::where('descricao', 'like', '%'.$_SESSION[$this->page]['search'].'%')
                ->limit(getenv('APP_LIMIT_PAGINATION'))
                ->offset($offset)
                ->orderBy('id', 'desc')
                ->get();
        }

        return Frequencias::limit(getenv('APP_LIMIT_PAGINATION'))->offset($offset)->orderBy('id', 'desc')->get();
    }

    private function count()
    {
        if (isset($_SESSION[$this->page]['search'])) {
            return Frequencias::where('descricao', 'like', '%'.$_SESSION[$this->page]['search'].'%')->count() ?? 1;
        }

        return Frequencias::count() ?? 1;
    }
}