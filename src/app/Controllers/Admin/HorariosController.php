<?php

namespace App\Controllers\Admin;


use System\Controller;
use App\Models\Horarios;
use App\Models\Logs;
use App\Helpers\Bootstrap\Pagination;
use Respect\Validation\Validator as v;

class HorariosController extends Controller
{
    public function __construct($container)
    {
        parent::__construct($container);
        $this->page = 'horarios';
    }

    public function index($request, $response, $args)
    {
        $data['servidor'] = (int)$args['servidor'];
        $data['tuplas'] = Horarios::where('servidores_id', $data['servidor'])->limit(5)->orderBy('semana', 'asc')->get();
        $data['form'] = Horarios::where('servidores_id', $data['servidor'])->count();
        $data['semana'] = $this->getSemana($args['servidor']);
        $data['search'] = $_SESSION[$this->page]['search'] ?? false;

        return $this->view->render($response, 'admin/'.$this->page.'/index.twig', $data);
    }

    private function getSemana($servidor)
    {
        $data = Horarios::select('semana')->where('servidores_id', $servidor)->get()->toArray();
        $a = [];

        foreach ($data as $value) {
            $a[] = $value['semana'];
        }

        return $a;
    }

    public function postCreate($request, $response, $args)
    {
        if (false === $request->getAttribute('csrf_status')) {
            $this->flash->addMessage('error', 'Erro ao realizar a requisição, tente novamente.');
            return $response->withRedirect($this->router->pathFor($this->page, ['servidor' => $args['servidor']]));
        }

        $rules = [
            'semana'  => v::notEmpty()->intVal(),
            'entrada_m'  => v::date('H:i:s'),
            'saida_m'  => v::date('H:i:s'),
            'servidores_id'  => v::notEmpty()->intVal(),
        ];

        if ($request->getParam('um_turno') == 'false') {
            $rules = array_merge($rules, [
                'entrada_t'  => v::date('H:i:s'),
                'saida_t'  => v::date('H:i:s'),
            ]);
        }

        $validation = $this->validator->validate($request, $rules);

        if ($validation->failed()) {
            return $response->withRedirect($this->router->pathFor($this->page, ['servidor' => $args['servidor']]));
        }

        $i = 1;
        do {
            if (in_array($i, $this->getSemana($args['servidor']))) {
                $i++;
                continue;
            }

            try {
                $reg = new Horarios();

                if ($request->getParam('semana') == 8) {
                    $reg->semana = $i++;
                } else {
                    $reg->semana = $request->getParam('semana');
                }

                $reg->entrada_m = $request->getParam('entrada_m');
                $reg->entrada_t = $request->getParam('entrada_t');
                $reg->saida_m = $request->getParam('saida_m');
                $reg->saida_t = $request->getParam('saida_t');
                $reg->servidores_id = $request->getParam('servidores_id');
                $reg->um_turno = $request->getParam('um_turno') == 'true'? 1: 0;
                $reg->save();
            } catch (\Exception $e) {
                die($e->getMessage());
            }
        } while ($request->getParam('semana') == 8 && $i <= 5);

        Logs::set("O usuário [{$this->auth->user()->usuario}] criou um novo registro de Horarios.");

        $this->flash->addMessage('success', 'Registro salvo com sucesso!');

        return $response->withRedirect($this->router->pathFor($this->page, ['servidor' => $args['servidor']]));
    }

    public function getDelete($request, $response, $args)
    {
        $id = (int) $args['id'];

        $reg = Horarios::find($id);

        if (!$reg) {
            $this->flash->addMessage('error', 'Erro ao realizar a requisição, tente novamente.');
            return $response->withRedirect($this->router->pathFor($this->page, ['servidor' => $args['servidor']]));
        }

        $reg->delete();

        Logs::set("O usuário [{$this->auth->user()->usuario}] apagou um registro de Horarios.");

        $this->flash->addMessage('success', 'Registro apagado com sucesso!');

        return $response->withRedirect($this->router->pathFor($this->page, ['servidor' => $args['servidor']]));
    }

    public function getEdit($request, $response, $args)
    {
        $id = (int) $args['id'];
        $numOfPage = $args['page'] ?? 1;
        $data['tuplas'] = $this->all($this->offset($numOfPage));
        $data['edit'] = Horarios::find($id);
        $data['form'] = true;
        $data['pagination'] = Pagination::render($this->count(), $args['page'] ?? 1, $this->router->pathFor($this->page, ['servidor' => $args['servidor']]));
        return $this->view->render($response, 'admin/'.$this->page.'/index.twig', $data);
    }

    public function postEdit($request, $response, $args)
    {
        if (false === $request->getAttribute('csrf_status')) {
            $this->flash->addMessage('error', 'Erro ao realizar a requisição, tente novamente.');
            return $response->withRedirect($this->router->pathFor($this->page, ['servidor' => $args['servidor']])."/editar/{$args['id']}");
        }

        $id = (int) $args['id'];

        $reg = Horarios::find($id);

        if (!$reg) {
            $this->flash->addMessage('error', 'Erro ao realizar a requisição, tente novamente.');
            return $response->withRedirect($this->router->pathFor($this->page, ['servidor' => $args['servidor']]));
        }

        $validation = $this->validator->validate($request, [
            'descricao'  => v::length(3,80)->notEmpty(),
        ]);

        if ($validation->failed()) {
            return $response->withRedirect($this->router->pathFor($this->page));
        }

        $reg->descricao  = $request->getParam('descricao');
        $reg->save();

        Logs::set("O usuário [{$this->auth->user()->usuario}] editou um registro de Horarios");

        $this->flash->addMessage('success', 'Registro atualizado com sucesso!');

        return $response->withRedirect($this->router->pathFor($this->page, ['servidor' => $args['servidor']]));
    }

    private function all($offset = 0)
    {
        if (isset($_SESSION[$this->page]['search'])) {
            return Horarios::where('descricao', 'like', '%'.$_SESSION[$this->page]['search'].'%')
                ->limit(getenv('APP_LIMIT_PAGINATION'))
                ->offset($offset)
                ->orderBy('id', 'desc')
                ->get();
        }

        return Horarios::limit(getenv('APP_LIMIT_PAGINATION'))->offset($offset)->orderBy('id', 'desc')->get();
    }

    private function count()
    {
        if (isset($_SESSION[$this->page]['search'])) {
            return Horarios::where('descricao', 'like', '%'.$_SESSION[$this->page]['search'].'%')->count() ?? 1;
        }

        return Horarios::count() ?? 1;
    }
}