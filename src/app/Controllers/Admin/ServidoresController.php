<?php

namespace App\Controllers\Admin;


use App\Models\Campus;
use App\Models\Cargos;
use App\Models\Funcoes;
use System\Controller;
use App\Models\Servidores;
use App\Models\Logs;
use App\Helpers\Bootstrap\Pagination;
use Respect\Validation\Validator as v;

class ServidoresController extends Controller
{
    public function __construct($container)
    {
        parent::__construct($container);
        $this->page = 'servidores';
    }

    public function index($request, $response, $args)
    {
        $numOfPage = $args['page'] ?? 1;
        $data['tuplas'] = $this->all($this->offset($numOfPage));
        $data['funcoes'] = Funcoes::all();
        $data['campus'] = Campus::all();
        $data['cargos'] = Cargos::all();
        $data['pagination'] = Pagination::render($this->count(), $args['page'] ?? 1, $this->router->pathFor($this->page));
        $data['search'] = $_SESSION[$this->page]['search'] ?? false;

        return $this->view->render($response, 'admin/'.$this->page.'/index.twig', $data);
    }

    public function postCreate($request, $response)
    {
        if (false === $request->getAttribute('csrf_status')) {
            $this->flash->addMessage('error', 'Erro ao realizar a requisição, tente novamente.');
            return $response->withRedirect($this->router->pathFor($this->page));
        }

        $validation = $this->validator->validate($request, [
            'nome'       => v::length(3,150)->notEmpty(),
            'matricula'  => v::intVal()->notEmpty(),
            'jornada'    => v::intVal()->notEmpty(),
            'funcoes_id' => v::intVal()->notEmpty(),
            'cargos_id'  => v::intVal()->notEmpty(),
            'campus_id'  => v::intVal()->notEmpty(),
        ]);

        if ($validation->failed()) {
            return $response->withRedirect($this->router->pathFor($this->page));
        }

        try {
            $reg = new Servidores();
            $reg->nome       = $request->getParam('nome');
            $reg->matricula  = $request->getParam('matricula');
            $reg->jornada    = $request->getParam('jornada');
            $reg->campus_id  = $request->getParam('campus_id');
            $reg->cargos_id  = $request->getParam('cargos_id');
            $reg->funcoes_id = $request->getParam('funcoes_id');
            $reg->save();
        } catch (\Exception $e) {
            die($e->getMessage());
        }

        Logs::set("O usuário [{$this->auth->user()->usuario}] criou um novo registro de Servidores.");

        $this->flash->addMessage('success', 'Registro salvo com sucesso!');

        return $response->withRedirect($this->router->pathFor($this->page));
    }

    public function getDelete($request, $response, $args)
    {
        $id = (int) $args['id'];

        $reg = Servidores::find($id);

        if (!$reg) {
            $this->flash->addMessage('error', 'Erro ao realizar a requisição, tente novamente.');
            return $response->withRedirect($this->router->pathFor($this->page));
        }

        $reg->delete();

        Logs::set("O usuário [{$this->auth->user()->usuario}] apagou um registro de Servidores.");

        $this->flash->addMessage('success', 'Registro apagado com sucesso!');

        return $response->withRedirect($this->router->pathFor($this->page));
    }

    public function getEdit($request, $response, $args)
    {
        $id = (int) $args['id'];
        $numOfPage = $args['page'] ?? 1;
        $data['tuplas'] = $this->all($this->offset($numOfPage));
        $data['funcoes'] = Funcoes::all();
        $data['campus'] = Campus::all();
        $data['cargos'] = Cargos::all();
        $data['edit'] = Servidores::find($id);
        $data['form'] = true;
        $data['pagination'] = Pagination::render($this->count(), $args['page'] ?? 1, $this->router->pathFor($this->page));
        return $this->view->render($response, 'admin/'.$this->page.'/index.twig', $data);
    }

    public function postEdit($request, $response, $args)
    {
        if (false === $request->getAttribute('csrf_status')) {
            $this->flash->addMessage('error', 'Erro ao realizar a requisição, tente novamente.');
            return $response->withRedirect($this->router->pathFor($this->page)."/editar/{$args['id']}");
        }

        $id = (int) $args['id'];

        $reg = Servidores::find($id);

        if (!$reg) {
            $this->flash->addMessage('error', 'Erro ao realizar a requisição, tente novamente.');
            return $response->withRedirect($this->router->pathFor($this->page));
        }

        $validation = $this->validator->validate($request, [
            'nome'       => v::length(3,150)->notEmpty(),
            'matricula'  => v::intVal()->notEmpty(),
            'jornada'    => v::intVal()->notEmpty(),
            'funcoes_id' => v::intVal()->notEmpty(),
            'cargos_id'  => v::intVal()->notEmpty(),
            'campus_id'  => v::intVal()->notEmpty(),
        ]);

        if ($validation->failed()) {
            return $response->withRedirect($this->router->pathFor($this->page));
        }

        $reg->nome       = $request->getParam('nome');
        $reg->matricula  = $request->getParam('matricula');
        $reg->jornada    = $request->getParam('jornada');
        $reg->campus_id  = $request->getParam('campus_id');
        $reg->cargos_id  = $request->getParam('cargos_id');
        $reg->funcoes_id = $request->getParam('funcoes_id');
        $reg->save();

        Logs::set("O usuário [{$this->auth->user()->usuario}] editou um registro de Servidores");

        $this->flash->addMessage('success', 'Registro atualizado com sucesso!');

        return $response->withRedirect($this->router->pathFor($this->page));
    }

    private function all($offset = 0)
    {
        if (isset($_SESSION[$this->page]['search'])) {
            return Servidores::where('nome', 'like', '%'.$_SESSION[$this->page]['search'].'%')
                ->limit(getenv('APP_LIMIT_PAGINATION'))
                ->offset($offset)
                ->orderBy('id', 'desc')
                ->get();
        }

        return Servidores::limit(getenv('APP_LIMIT_PAGINATION'))->offset($offset)->orderBy('id', 'desc')->get();
    }

    private function count()
    {
        if (isset($_SESSION[$this->page]['search'])) {
            return Servidores::where('nome', 'like', '%'.$_SESSION[$this->page]['search'].'%')->count() ?? 1;
        }

        return Servidores::count() ?? 1;
    }
}