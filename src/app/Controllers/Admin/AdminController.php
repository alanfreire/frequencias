<?php

namespace App\Controllers\Admin;

use App\Models\Frequencias;
use App\Models\Logs;
use App\Models\Servidores;
use System\Controller;
use Slim\Http\Request;
use Slim\Http\Response;

class AdminController extends Controller
{
    public function __construct($container)
    {
        parent::__construct($container);
    }

    public function index(Request $request, Response $response, array $args)
    {
        $data['servidores'] = Servidores::orderBy('id', 'desc')->limit(8)->get();
        $data['frequencias'] = Frequencias::orderBy('id', 'desc')->limit(8)->get();
        $data['logs'] = Logs::where('usuarios_id', $_SESSION['user'])->orderBy('id', 'desc')->limit(8)->get();
        return $this->view->render($response, 'admin/index.twig', $data);
    }
}