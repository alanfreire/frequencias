<?php

namespace App\Controllers\Admin;

use App\Helpers\Bootstrap\Pagination;
use System\Controller;
use App\Models\Usuarios;
use App\Models\Group;
use App\Models\Site;
use App\Models\Authorization;
use App\Models\Logs;
use Respect\Validation\Validator as v;

class UsuariosController extends Controller
{
    public function __construct($container)
    {
        parent::__construct($container);
        $this->page = 'usuarios';
    }

    public function index($request, $response, $args)
    {
        $numOfPage = $args['page'] ?? 1;
        $data['tuplas'] = $this->all($this->offset($numOfPage));
        $data['pagination'] = Pagination::render($this->count(), $args['page'] ?? 1, $this->router->pathFor($this->page));
        $data['search'] = $_SESSION[$this->page]['search'] ?? false;

        return $this->view->render($response, 'admin/usuarios/index.twig', $data);
    }

    public function postCreate($request, $response)
    {
        if (false === $request->getAttribute('csrf_status')) {
            $this->flash->addMessage('error', 'Erro ao realizar a requisição, tente novamente.');
            return $response->withRedirect($this->router->pathFor($this->page));
        }

        $validation = $this->validator->validate($request, [
            'usuario'    => v::length(3)->noWhitespace()->notEmpty()->userAvailable(),
            'nivel'      => v::notEmpty(),
            'senha'      => v::length(6)->noWhitespace()->notEmpty(),
            'senha_conf' => v::passConfirmation($request->getParam('senha')),
            'situacao'   => v::notBlank(),
        ]);

        if ($validation->failed()) {
            return $response->withRedirect($this->router->pathFor($this->page));
        }

        try {
            $user = new Usuarios();
            $user->usuario  = $request->getParam('usuario');
            $user->nivel    = $request->getParam('nivel');
            $user->situacao = $request->getParam('situacao') == 'true'? true: false;
            $user->senha    = password_hash($request->getParam('senha'), PASSWORD_DEFAULT);
            $user->save();
        } catch (\Exception $e) {
            die($e->getMessage());
        }

        Logs::set("O usuário [{$this->auth->user()->usuario}] criou um novo usuario: [{$user->id}-{$user->usuario}].");

        $this->flash->addMessage('success', 'Registro salvo com sucesso!');

        return $response->withRedirect($this->router->pathFor($this->page));
    }

    public function getDelete($request, $response, $args)
    {
        $id = (int) $args['id'];

        $reg = Usuarios::find($id);

        if (!$reg) {
            $this->flash->addMessage('error', 'Erro ao realizar a requisição, tente novamente.');
            return $response->withRedirect($this->router->pathFor($this->page));
        }

        $reg->delete();

        Logs::set("O usuário [{$this->auth->user()->usuario}] apagou um registro de Usuário.");

        $this->flash->addMessage('success', 'Registro apagado com sucesso!');

        return $response->withRedirect($this->router->pathFor($this->page));
    }

    public function getEdit($request, $response, $args)
    {
        $id = (int) $args['id'];
        $numOfPage = $args['page'] ?? 1;
        $data['tuplas'] = $this->all($this->offset($numOfPage));
        $data['edit'] = Usuarios::find($id);
        $data['form'] = true;
        $data['pagination'] = Pagination::render(Usuarios::count(), $args['page'] ?? 1, $this->router->pathFor($this->page));
        return $this->view->render($response, 'admin/usuarios/index.twig', $data);
    }

    public function postEdit($request, $response, $args)
    {
        if (false === $request->getAttribute('csrf_status')) {
            $this->flash->addMessage('error', 'Erro ao realizar a requisição, tente novamente.');
            return $response->withRedirect($this->router->pathFor($this->page)."/editar/{$args['id']}");
        }

        $id = (int) $args['id'];

        $reg = Usuarios::find($id);

        if (!$reg) {
            $this->flash->addMessage('error', 'Erro ao realizar a requisição, tente novamente.');
            return $response->withRedirect($this->router->pathFor($this->page));
        }

        $validation = $this->validator->validate($request, [
            'usuario'  => v::length(3)->noWhitespace()->notEmpty(),
            'nivel'    => v::notEmpty(),
            'situacao' => v::notBlank(),
        ]);

        if ($validation->failed()) {
            return $response->withRedirect($this->router->pathFor($this->page));
        }

        $reg->usuario  = $request->getParam('usuario');
        $reg->nivel    = $request->getParam('nivel');
        $reg->situacao = $request->getParam('situacao') == 'true'? true: false;
        $reg->save();

        Logs::set("O usuário [{$this->auth->user()->usuario}] editou um registro de Usuário");

        $this->flash->addMessage('success', 'Registro atualizado com sucesso!');

        return $response->withRedirect($this->router->pathFor($this->page));
    }

    public function postPassword($request, $response, $args)
    {
        if (false === $request->getAttribute('csrf_status')) {
            $this->flash->addMessage('error', 'Erro ao realizar a requisição, tente novamente.');
            return $response->withRedirect($this->router->pathFor($this->page)."/editar/{$args['id']}");
        }

        if ($request->getParam('senha') !== $request->getParam('senha_conf')) {
            $this->flash->addMessage('error', 'Os campos senha e confirmação de senha devem ser iguais.');
            return $response->withRedirect($this->router->pathFor($this->page));
        }

        if ($request->getParam('senha') == '' || $request->getParam('senha_conf') == '') {
            $this->flash->addMessage('error', 'Os campos senha e confirmação de senha são obrigatórios.');
            return $response->withRedirect($this->router->pathFor($this->page));
        }

        $id = (int) $args['id'];
        $user = Usuarios::find($id);

        if (!$user) {
            $this->flash->addMessage('error', 'Usuário não encontrado.');
            return $response->withRedirect($this->router->pathFor($this->page));
        }

        $user->senha = password_hash($request->getParam('senha'), PASSWORD_DEFAULT);
        $user->save();

        Logs::set("O usuário [{$this->auth->user()->usuario}] alterou a senha do usuário [{$id}-{$user->usuario}].");

        $this->flash->addMessage('success', 'Registro alterado com sucesso!');

        return $response->withRedirect($this->router->pathFor($this->page));
    }

    private function all($offset = 0)
    {
        if (isset($_SESSION[$this->page]['search'])) {
            return Usuarios::where('usuario', 'like', '%'.$_SESSION[$this->page]['search'].'%')
                ->limit(getenv('APP_LIMIT_PAGINATION'))
                ->offset($offset)
                ->orderBy('id', 'desc')
                ->get();
        }

        return Usuarios::limit(getenv('APP_LIMIT_PAGINATION'))->offset($offset)->orderBy('id', 'desc')->get();
    }

    private function count()
    {
        if (isset($_SESSION[$this->page]['search'])) {
            return Usuarios::where('usuario', 'like', '%'.$_SESSION[$this->page]['search'].'%')
                    ->count() ?? 1;
        }

        return Usuarios::count() ?? 1;
    }
}