<?php

namespace App\Controllers\Admin;


use System\Controller;
use App\Models\FeriadosTipos;
use App\Models\Logs;
use App\Helpers\Bootstrap\Pagination;
use Respect\Validation\Validator as v;

class FeriadosTiposController extends Controller
{
    public function __construct($container)
    {
        parent::__construct($container);
        $this->page = 'feriadosTipos';
    }

    public function index($request, $response, $args)
    {
        $numOfPage = $args['page'] ?? 1;
        $data['tuplas'] = $this->all($this->offset($numOfPage));
        $data['pagination'] = Pagination::render($this->count(), $args['page'] ?? 1, $this->router->pathFor($this->page));
        $data['search'] = $_SESSION[$this->page]['search'] ?? false;

        return $this->view->render($response, 'admin/'.$this->page.'/index.twig', $data);
    }

    public function postCreate($request, $response)
    {
        if (false === $request->getAttribute('csrf_status')) {
            $this->flash->addMessage('error', 'Erro ao realizar a requisição, tente novamente.');
            return $response->withRedirect($this->router->pathFor($this->page));
        }

        $validation = $this->validator->validate($request, [
            'descricao'  => v::length(3,80)->notEmpty(),
        ]);

        if ($validation->failed()) {
            return $response->withRedirect($this->router->pathFor($this->page));
        }

        try {
            $user = new FeriadosTipos();
            $user->descricao  = $request->getParam('descricao');
            $user->save();
        } catch (\Exception $e) {
            die($e->getMessage());
        }

        Logs::set("O usuário [{$this->auth->user()->usuario}] criou um novo registro de Tipos de Feriados.");

        $this->flash->addMessage('success', 'Registro salvo com sucesso!');

        return $response->withRedirect($this->router->pathFor($this->page));
    }

    public function getDelete($request, $response, $args)
    {
        $id = (int) $args['id'];

        $reg = FeriadosTipos::find($id);

        if (!$reg) {
            $this->flash->addMessage('error', 'Erro ao realizar a requisição, tente novamente.');
            return $response->withRedirect($this->router->pathFor($this->page));
        }

        $reg->delete();

        Logs::set("O usuário [{$this->auth->user()->usuario}] apagou um registro de Tipos de Feriados.");

        $this->flash->addMessage('success', 'Registro apagado com sucesso!');

        return $response->withRedirect($this->router->pathFor($this->page));
    }

    public function getEdit($request, $response, $args)
    {
        $id = (int) $args['id'];
        $numOfPage = $args['page'] ?? 1;
        $data['tuplas'] = $this->all($this->offset($numOfPage));
        $data['edit'] = FeriadosTipos::find($id);
        $data['form'] = true;
        $data['pagination'] = Pagination::render($this->count(), $args['page'] ?? 1, $this->router->pathFor($this->page));
        return $this->view->render($response, 'admin/'.$this->page.'/index.twig', $data);
    }

    public function postEdit($request, $response, $args)
    {
        if (false === $request->getAttribute('csrf_status')) {
            $this->flash->addMessage('error', 'Erro ao realizar a requisição, tente novamente.');
            return $response->withRedirect($this->router->pathFor($this->page)."/editar/{$args['id']}");
        }

        $id = (int) $args['id'];

        $reg = FeriadosTipos::find($id);

        if (!$reg) {
            $this->flash->addMessage('error', 'Erro ao realizar a requisição, tente novamente.');
            return $response->withRedirect($this->router->pathFor($this->page));
        }

        $validation = $this->validator->validate($request, [
            'descricao'  => v::length(3,80)->notEmpty(),
        ]);

        if ($validation->failed()) {
            return $response->withRedirect($this->router->pathFor($this->page));
        }

        $reg->descricao  = $request->getParam('descricao');
        $reg->save();

        Logs::set("O usuário [{$this->auth->user()->usuario}] editou um registro de Tipos de Feriados");

        $this->flash->addMessage('success', 'Registro atualizado com sucesso!');

        return $response->withRedirect($this->router->pathFor($this->page));
    }

    private function all($offset = 0)
    {
        if (isset($_SESSION[$this->page]['search'])) {
            return FeriadosTipos::where('descricao', 'like', '%'.$_SESSION[$this->page]['search'].'%')
                ->limit(getenv('APP_LIMIT_PAGINATION'))
                ->offset($offset)
                ->orderBy('id', 'desc')
                ->get();
        }

        return FeriadosTipos::limit(getenv('APP_LIMIT_PAGINATION'))->offset($offset)->orderBy('id', 'desc')->get();
    }

    private function count()
    {
        if (isset($_SESSION[$this->page]['search'])) {
            return FeriadosTipos::where('descricao', 'like', '%'.$_SESSION[$this->page]['search'].'%')->count() ?? 1;
        }

        return FeriadosTipos::count() ?? 1;
    }
}