<?php

namespace App\Controllers\Admin;


use App\Models\Usuarios;
use System\Controller;
use App\Models\Logs;
use App\Helpers\Bootstrap\Pagination;

class LogsController extends Controller
{
    public function __construct($container)
    {
        parent::__construct($container);
        $this->page = 'logs';
    }

    public function index($request, $response, $args)
    {
        $numOfPage = $args['page'] ?? 1;
        $data['usuario'] = Usuarios::find($args['usuario']);
        $data['tuplas'] = $this->all($this->offset($numOfPage), $args['usuario']);
        $data['pagination'] = Pagination::render($this->count($args['usuario']), $args['page'] ?? 1, $this->router->pathFor($this->page, ['usuario'=>$data['usuario']->id]));
        $data['search'] = $_SESSION[$this->page]['search'] ?? false;

        return $this->view->render($response, 'admin/logs/index.twig', $data);
    }

    public function getDelete($request, $response, $args)
    {
        $id = (int)$args['id'];
        $user = (int)$args['usuario'];
        $reg = Logs::find($id);

        if (!$reg) {
            $this->flash->addMessage('error', 'Erro ao realizar a requisição, tente novamente.');
            return $response->withRedirect($this->router->pathFor($this->page, ['usuario'=>$user]));
        }

        $reg->delete();

        Logs::set("O usuário [{$this->auth->user()->usuario}] apagou um registro de Log.");

        $this->flash->addMessage('success', 'Registro apagado com sucesso!');

        return $response->withRedirect($this->router->pathFor($this->page,  ['usuario'=>$user]));
    }

    private function all($offset = 0, $usuario)
    {
        return Logs::where('usuarios_id', $usuario)->limit(getenv('APP_LIMIT_PAGINATION'))->offset($offset)->orderBy('id', 'desc')->get();
    }

    private function count($usuario)
    {
        return Logs::where('usuarios_id', $usuario)->count() ?? 1;
    }
}