<?php

namespace App\Controllers;

use App\Models\Feriados;
use App\Models\Frequencias;
use App\Models\Horarios;
use App\Models\Servidores;
use Couchbase\Exception;
use Mpdf\Mpdf;
use System\Controller;
use Slim\Http\Request;
use Slim\Http\Response;

class HomeController extends Controller
{
    public function __construct($container)
    {
        parent::__construct($container);
    }

    public function index(Request $request, Response $response, array $args)
    {
        //$this->logger->error("SCF '/' Home");
        $data['matricula'] = $args['matricula'];
        return $this->view->render($response, 'index.twig', $data);
    }

    public function post(Request $request, Response $response, array $args)
    {
        $this->validateCsrf($request, $response);

        $data['ano'] = (int) $request->getParam('ano');
        $data['mes'] = (int) $request->getParam('mes');
        $data['matricula'] = $args['matricula'];
        $data['servidor'] = Servidores::where('matricula', (int)$request->getParam('matricula'))->first();
        $data['frequencias'] = Frequencias::where('servidores_id', $data['servidor']->id)->limit(12)->orderBy('id', 'desc')->get();

        return $this->view->render($response, 'index.twig', $data);
    }

    public function getPrint(Request $request, Response $response, array $args)
    {
        $pdf = new Mpdf(['tempDir'=>ROOT_DIR.'/src/app/Views/cache']);
        $pdf->WriteHTML(file_get_contents(sprintf('%s/imprimir/html/%s/%s/%s/', getenv('APP_URL'), $args['mes'], $args['ano'], $args['matricula'])));
        $pdf->Output(str_replace(' ', '_', Servidores::where('matricula', $args['matricula'])->first()->nome).'-'.$args['mes'].'-'.$args['ano'].'.pdf', \Mpdf\Output\Destination::INLINE);
        return $response->withHeader('Content-Type', 'application/pdf');
    }

    public function getHtml(Request $request, Response $response, array $args)
    {
        $data['servidor'] = Servidores::where('matricula', $args['matricula'])->first();
        $data['horarios'] = Horarios::where('servidores_id', $data['servidor']->id)->get();
        $data['frequencia'] = $this->getDias($args);
        $data['ano'] = $args['ano'];
        $data['mes'] = $args['mes'];

        return $this->view->render($response, 'frequencia.twig', $data);
    }

    private function getDias($args)
    {
        $totalDias = cal_days_in_month(CAL_GREGORIAN, $args['mes'], $args['ano']);
        $frequencia = [];

        foreach (range(1, $totalDias) as $dia) {
            $tmpFrequencia['dia'] = $dia;
            $tmpFrequencia['diaSemana'] = jddayofweek(gregoriantojd($args['mes'], $dia, $args['ano']));

            $frequencia[] = array_merge($tmpFrequencia, $this->getFeriado($dia, $args['mes'], $args['ano']));
        }

        return $frequencia;
    }

    private function getFeriado($dia, $mes, $ano)
    {
        $data = date('Y-m-d', mktime(0,0,0, $mes, $dia, $ano));
        $feriado = Feriados::where('data', $data)->first();

        if (count($feriado)) {
            return ['feriado' => true, 'feriadoTipo' => $feriado->feriadosTipos->descricao];
        }

        return ['feriado' => false, 'feriadoTipo' => false];
    }

    public function getFrequencias(Request $request, Response $response, array $args)
    {

    }
}