<?php

namespace App\Controllers;

use System\Controller;
use Slim\Http\Request;
use Slim\Http\Response;
use App\Models\Logs;
use App\Models\Usuarios;

class AuthController extends Controller
{
    /**
     * getSignIn
     *
     * Mostra a página de login
     *
     * @param Request $request
     * @param Response $response
     * @return object twig
     */
    public function getSignIn(Request $request, Response $response)
    {
        return $this->view->render($response, 'auth/index.twig');
    }

    /**
     * getSignOut
     *
     * Encerra um acesso
     *
     * @param Request $request
     * @param Response $response
     * @return object Response
     */
    public function getSignOut(Request $request, Response $response)
    {
        if (!$this->auth->check()) {
            return $response->withRedirect($this->router->pathFor('auth.signin'));
        }

        Logs::set("O usuário [{$this->auth->user()->usuario}] saiu do sistema.", $_SESSION['user']);

        $this->auth->logout();
        $this->flash->addMessage('info', 'Você está desconectado.');

        return $response->withRedirect($this->router->pathFor('auth.signin'));
    }

    /**
     * postSignIn
     *
     * Faz o login de um usário
     *
     * @param Request $request
     * @param Response $response
     * @return object Response
     */
    public function postSignIn(Request $request, Response $response, array $args)
    {
        if (false === $request->getAttribute('csrf_status')) {
            $this->flash->addMessage('error', 'Erro ao realizar a requisição, tente novamente.');
            return $response->withRedirect($this->router->pathFor('auth.signin'));
        }

        $auth = $this->auth->attempt(
            $request->getParam('usuario'),
            $request->getParam('senha')
        );

        if (!$auth) {
            $this->flash->addMessage('error', 'Usuário ou senha incorretos.');
            return $response->withRedirect($this->router->pathFor('auth.signin'));
        }

        Logs::set("O usuário [{$this->auth->user()->usuario}] entrou no sistema.", $_SESSION['user']);

        $this->flash->addMessage('success', 'Login efetuado com sucesso.');
        return $response->withRedirect($this->router->pathFor('admin'));
    }
}