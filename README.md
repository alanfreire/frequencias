# SCF
### Sistema de Controle de Frequências
Sistema desenvolvido para gerar a frequência mensal dos servidores do IFCE/Campus Baturité

# Instalação
### Passo 1
Execute o ``composer`` para instação das dependências das bibliotecas no diretorio que foi baixado os arquivos.

Comando: ``composer install``

### Passo 2
Importe o arquivo ``scf.sql`` para o banco de dados MySQL/MariaDB

O arquivo se encontra no diretório ``frequencias/database``. Dentro deste diretorio também há um arquivo do Modelo Entidade Relacionamento que foi feito no MySQLWorkBench para você que queira fazer alguma modificação nos dados.

### Passo 3
Edite o arquivo ``.env`` que está no diretório ``frequencias/src/app``

Coloque as configurações do seu banco de dados e URL do sistema.

### Passo 4
Para acessar o sistema use o usuário ``admin`` e senha ``qwe123``
   
# Versão
2.0.0

# Licença
MIT © 2018 Alan Freire
